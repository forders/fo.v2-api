import {
    db
} from '../../modules';
import {
    logger
} from '../../modules'

const countryModel = db.default.loadSchema('tbl_country', {
    fields: {
        _key:{
            type: "uuid",
            default: {
                "$db_function": "uuid()"
            }
        },
        countryid: "varchar",
        countrycode:"varchar",
        countryname: "varchar",
        baseccy:"varchar",
        status: "varchar",
        createdby: "varchar",
        updatedby:"varchar"
    },
    options: {
        timestamps: {
            createdAt: 'createddt', // defaults to createdAt
            updatedAt: 'updateddt' // defaults to updatedAt
        },
        versions: {
            key: '__v' // defaults to __v
        }
    },
    key: [["_key"]],
    indexes:["countryid"]
});

countryModel.syncDB(function (err, result) {
    if (err) {
        logger.log.error("Model Error for country",err);
        throw err
    };
    // result == true if any database schema was updated
    // result == false if no schema change was detected in your models
});

export {
    countryModel
}