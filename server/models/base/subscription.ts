import {
    db
} from '../../modules';
import {
    logger
} from '../../modules'

const subscriptionModel = db.default.loadSchema('tbl_subscriptions', {
    fields: {
        _key: {
            type: "uuid",
            default: {
                "$db_function": "uuid()"
            }
        },
        subscriptionid:"varchar",
        tenantid: "varchar",
        tenure: "varchar",
        subsamount: "varchar",
        lastpaidamount: "varchar",
        lastpaiddate: "varchar",
        nextduedate: "varchar",
        updatedby: "varchar",
        updateddt: "varchar"
    },
    options: {
        timestamps: {
            createdAt: 'createddt', // defaults to createdAt
            updatedAt: 'updateddt' // defaults to updatedAt
        },
        versions: {
            key: '__v' // defaults to __v
        }
    },
    key: [["_key"]]
});

subscriptionModel.syncDB(function (err, result) {
    if (err) {
        logger.log.error("Model Error for roles",err);
        throw err
    };
    // result == true if any database schema was updated
    // result == false if no schema change was detected in your models
});

export {
    subscriptionModel
}