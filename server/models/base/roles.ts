import {
    db
} from '../../modules';
import {
    logger
} from '../../modules'

const roleModel = db.default.loadSchema('tbl_roles', {
    fields: {
        _key: {
            type: "uuid",
            default: {
                "$db_function": "uuid()"
            }
        },
        roleid:"varchar",
        rolename: "varchar",
        tenantid: "varchar",
        uiactions: "varchar",
        dataaccess: "varchar",
        status: "varchar",
        createdby: "varchar",
        updatedby:"varchar"
    },
    options: {
        timestamps: {
            createdAt: 'createddt', // defaults to createdAt
            updatedAt: 'updateddt' // defaults to updatedAt
        },
        versions: {
            key: '__v' // defaults to __v
        }
    },
    key: [["_key"],"roleid"]
});

roleModel.syncDB(function (err, result) {
    if (err) {
        logger.log.error("Model Error for roles",err);
        throw err
    };
    // result == true if any database schema was updated
    // result == false if no schema change was detected in your models
});

export {
    roleModel
}