import {
    db
} from '../../modules';
import {
    logger
} from '../../modules';

const tenantModel = db.default.loadSchema('tbl_tenant', {
    fields: {
        _key: {
            type: "uuid",
            default: {
                "$db_function": "uuid()"
            }
        },
        tenantid:"varchar",
        tenantname:"varchar",
        logo: "varchar",
        industryid: "varchar",
        countryid: "varchar",
        emailid: "varchar",
        mobileno: "varchar",
        address: "varchar",
        taxno: "varchar",
        phoneno: "varchar",
        associations: "varchar",
        paidtenantyn: "varchar",
        remarks: "varchar",
        subscriptionstatus: "varchar",
        renewalduedt: "varchar",
        status: "varchar",
        createdby: "varchar",
        updatedby: "varchar"
    },
    options: {
        timestamps: {
            createdAt: 'createddt', // defaults to createdAt
            updatedAt: 'updateddt' // defaults to updatedAt
        },
        versions: {
            key: '__v' // defaults to __v
        }
    },
    key: [["_key"]]
});

tenantModel.syncDB(function (err, result) {
    if (err) {
        logger.log.error("Model Error for Tenants Table",err);
        throw err
    };
    // result == true if any database schema was updated
    // result == false if no schema change was detected in your models
});

export {
    tenantModel
}