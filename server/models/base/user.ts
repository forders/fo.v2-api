import {
    db
} from '../../modules';
import {
    logger
} from '../../modules';

const userModel = db.default.loadSchema('tbl_users', {
    fields: {
        _key: {
            type: "uuid",
            default: {
                "$db_function": "uuid()"
            }
        },
        userid:"varchar",
        tenantid: "varchar",
        roleid: "varchar",
        password: "varchar",
        profileimage: "varchar",
        fullname: "varchar",
        emailid: "varchar",
        mobileno: "varchar",
        lastlogin: {
            type: "timestamp",
            default: { "$db_function": "toTimestamp(now())" }
        },
        status: "varchar",
        createdby: "varchar",
        updatedby: "varchar"
    },
    options: {
        timestamps: {
            createdAt: 'createddt', // defaults to createdAt
            updatedAt: 'updateddt' // defaults to updatedAt
        },
        versions: {
            key: '__v' // defaults to __v
        }
    },
    key: [["_key"]]
});

userModel.syncDB(function (err, result) {
    if (err) {
        logger.log.error("Model Error for Users Table",err);
        throw err
    };
    // result == true if any database schema was updated
    // result == false if no schema change was detected in your models
});

export {
    userModel
}