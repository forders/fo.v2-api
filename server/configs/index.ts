import db_conf from './db';
import global_conf from './global';
import res_msg from './responsemsg';

export {
    db_conf,
    global_conf,
    res_msg
};