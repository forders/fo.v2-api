export default  {
    AUTH:{
        SIGNIN:{
            UNOTFOUND:"User not found!",
            PWDNOTVALID:"Incorrect Password",
            SUCCESS:"Logged In Successfully",
            FGTPWDSUC:"Password sent to your mobile"
        },
        SIGNUP:{
            SUCCESS:"Signup Completed",
            FAILED:"Unable to signup right now!"
        }
    },
    ROLES:{
        NEW:{
            SUC:"New role created successfully.",
            FAILED:"Unable to create new role"
        }
    },
    TENANT:{
        NEW:{
            SUC:"Thanks for signing up with us.",
            FAILED:"Unable to create new account for you."
        }
    },
    ADDSUCCESS:"{{}} created successfully",
    ADDFAILED:"Unable to add {{}}",
    UPDATESUCCESS:"{{}} updated successfully",
    UPDATEFAILED:"Unable to update {{}}"
};