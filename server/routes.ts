import { Application } from 'express';

// Grouped by Models
// Auth & User has signup API's
import authRouter from './api/controllers/base/auth/router';
import userRouter from './api/controllers/base/users/router';

import roleRouter from './api/controllers/base/roles/router';

import countryRouter from './api/controllers/base/country/router';

import industryRouter from './api/controllers/base/industry/router';

import tenantRouter from './api/controllers/base/tenants/router';

export default function routes(app: Application): void {
  app.use('/fo/api/auth', authRouter);
  app.use('/fo/api/role', roleRouter);
  app.use('/fo/api/country', countryRouter);
  app.use('/fo/api/industry', industryRouter);
  app.use('/fo/api/tenant', tenantRouter);
  app.use('/fo/api/user',userRouter);
};