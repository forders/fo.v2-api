import ExpressCassandra from 'express-cassandra';

import {
    db_conf
} from '../configs';

var models = ExpressCassandra.createClient({
    clientOptions: {
        contactPoints: db_conf.CONN_POINTS,
        protocolOptions: db_conf.CONN_PROTOCOLS,
        keyspace: db_conf.DEFAULT_KEYSPACE,
        queryOptions: {
            consistency: ExpressCassandra.consistencies.one
        }
    },
    ormOptions: {
        defaultReplicationStrategy: {
            class: 'SimpleStrategy',
            replication_factor: 1
        },
        migration: 'safe',
    }
});

export function genuuid(){
    return models.uuid();
}

export function uuifFromString(uid:String){
    let uuid = models.datatypes.Uuid.fromString(uid);
    return uuid;
}

export default models;

