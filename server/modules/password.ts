import {
    global_conf
} from '../configs'
import crypto from 'crypto';

export function encrypt(password) {
    const cipher = crypto.createCipher('aes-256-cbc', global_conf.SECRETS.PWD_ENC);

    let encrypted = cipher.update(password.toString(), 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return encrypted;
}
export function decrypt(hash) {
    const decipher = crypto.createDecipher('aes-256-cbc', global_conf.SECRETS.PWD_ENC);

    let decrypted = decipher.update(hash.toString(), 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}
export function verifypassword(hash, password) {
    const decipher = crypto.createDecipher('aes-256-cbc', global_conf.SECRETS.PWD_ENC);

    let decrypted = decipher.update(hash.toString(), 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    if (decrypted == password) return true;
    else return false;
}