import {
    Response
} from 'express';

export default function respond(success, data, message, err, res: Response) {
    res.send({
        success: success,
        data: data,
        message: message,
        err: err
    })
}