import {
    db,
    logger
} from '../../modules';

export class CommonService {
    get(table, query, options) {
        return new Promise((resolve, reject) => {
            table.find(query, options, function (err, results) {
                if (err) {
                    logger.log.error(`Error -> cquery `, err);
                    reject(err);
                } else {
                    resolve(results);
                }
            })
        })
    };
    findOne(table, filter, options) {
        return new Promise((resolve, reject) => {
            table.findOne(filter, options, function (err, results) {
                if (err) {
                    logger.log.error(`Error -> cquery `, err);
                    reject(err);
                } else {
                    resolve(results);
                }
            })
        })
    };
    addOne(table, data) {
        let coll = new table(data);
        return new Promise((resolve, reject) => {
            coll.save(function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            })
        })
    };
    updateOne(table, query, value, options) {
        return new Promise((resolve, reject) => {
            table.update(query, value, options, function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            })
        })
    }
}

export default new CommonService();