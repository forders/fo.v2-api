import { Request, Response } from 'express';
import {
    roleModel
} from '../../../../models';
import {
    respond,
    cquery
} from '../../../services'
import {
    logger,
    db
} from '../../../../modules'
import { res_msg } from '../../../../configs';

export default function addrole(req: Request, res: Response) {
    let roleid = db.genuuid();
    let newRole = req.body;
    newRole["_key"] = roleid;
    newRole["roleid"] = roleid.toString();

    logger.log.info('Adding new role...');
    let doc = cquery.addOne(roleModel, newRole);
    doc.then(() => {
        logger.log.info("...new role added",newRole["roleid"]);
        respond(true, "", res_msg.ROLES.NEW.SUC, "", res);
    });
    doc.catch((err) => {
        logger.log.error("...error adding new role", err, req.body);
        respond(false, "", res_msg.ROLES.NEW.FAILED, err, res);
    })
}