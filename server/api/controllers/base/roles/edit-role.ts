import { Request, Response } from 'express';
import {
    roleModel
} from '../../../../models';
import {
    respond,
    cquery
} from '../../../services'
import {
    logger,
    db
} from '../../../../modules'
import { res_msg } from '../../../../configs';

export default function editrole(req: Request, res: Response) {
    let data = req.body;
    let id = Buffer.from(req.params.id, 'base64').toString();
    let q = {
        roleid: id,
        _key: db.uuifFromString(id)
    }
    let options = {
        ttl: 86400, if_exists: true
    }
    logger.log.info("Updating role...", id);
    let role = cquery.updateOne(roleModel, q, data, options)
    role.then(() => {
        logger.log.info("...role updated.", id);
        respond(true, "", "Updated successfully", "", res);
    });
    role.catch(err => {
        logger.log.error("...err updating role.", err, req.body, id);
        respond(false, "", "Unable to update", err, res);
    })
}