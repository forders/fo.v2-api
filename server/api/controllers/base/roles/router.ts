import express from 'express';

import addRole from './add-role';
import editRole from './edit-role';
import {
    getAll,
    getById
} from './get-role';

export default express.Router()
    .post('/add', addRole)
    .post('/edit/:id', editRole)
    .get('/', getAll)
    .get('/:id', getById)