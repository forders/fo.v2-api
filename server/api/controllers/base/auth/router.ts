import express from 'express';

import signup from './signup';
import signin from './signin';
import forgotpassword from './forgotpassword';

export default express.Router()
    .post('/signin', signin)
    .post('/forgotpassword', forgotpassword)
    .post('/signup', signup);