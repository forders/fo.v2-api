import { Request, Response } from 'express';
import {
    userModel
} from '../../../../models';
import {
    pwd,
    logger
} from '../../../../modules';
import {
    res_msg
} from '../../../../configs'
import {
    cquery,
    respond
} from '../../../services';

export default function forgotpassword(req: Request, res: Response) {
    var user = cquery.findOne(userModel, {
        mobileno: req.body.mobileno
    }, {
            allow_filtering: true
        });

    logger.log.info("Forgot Password API started...")
    user.then(u => {
        let userr: any = u;
        if (userr == undefined) respond(false, "", res_msg.AUTH.SIGNIN.UNOTFOUND, "", res);
        else {
            let user_data = userr.toJSON();
            let password = pwd.decrypt(user_data["password"]);
            logger.log.info("...Password sent to mobile");
            respond(true, "", res_msg.AUTH.SIGNIN.FGTPWDSUC, "", res);
        }
    });
    user.catch(err => {
        logger.log.error("...error in sending password to mobile.", err, req.body);
    })
}