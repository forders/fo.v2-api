import { Request, Response } from 'express';
import {
    userModel
} from '../../../../models';
import {
    respond,
    cquery
} from '../../../services'
import {
    pwd,
    logger,
    db
} from '../../../../modules'
import { res_msg } from '../../../../configs';

export default function signup(req: Request, res: Response) {
    let newUser = req.body;
    newUser["password"] = pwd.encrypt(newUser['password']);

    logger.log.info(`Signing up user...`);

    let doc = cquery.addOne(userModel, newUser);
    doc.then((data) => {
        logger.log.info("...user signed up successgully");
        respond(true, "", res_msg.AUTH.SIGNUP.SUCCESS, "", res);
    })
    doc.catch(err => {
        logger.log.error("...unable to signup user", err, req.body);
        respond(false, "", res_msg.AUTH.SIGNUP.FAILED, err, res);
    })
}