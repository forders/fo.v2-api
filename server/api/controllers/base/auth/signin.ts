import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import {
    userModel
} from '../../../../models';
import {
    pwd,
    logger
} from '../../../../modules';
import {
    res_msg,
    global_conf
} from '../../../../configs'
import {
    cquery,
    respond
} from '../../../services';

export default function signin(req: Request, res: Response) {
    var user = cquery.findOne(userModel, {
        mobileno: req.body.mobileno
    }, {
            allow_filtering: true
        });
    logger.log.info("User signin request initialized...");
    user.then(u => {
        let userr: any = u;
        if (userr == undefined) { logger.log.error("...user not found.", req.body.mobileno); respond(false, "", res_msg.AUTH.SIGNIN.UNOTFOUND, "", res); }
        else {
            let user_data = userr.toJSON();
            if (pwd.verifypassword(user_data["password"], req.body.password)) {
                let response_data = {
                    user: userr.toJSON(),
                    token: jwt.sign(userr.toJSON(), global_conf.SECRETS.PWD_ENC)
                }
                logger.log.info("...user signed in successfully.",req.body.mobileno);
                respond(true, response_data, res_msg.AUTH.SIGNIN.SUCCESS, "", res);
            } else {
                logger.log.info("...password incorrect.",req.body.mobileno);
                respond(false, "", res_msg.AUTH.SIGNIN.PWDNOTVALID, "", res);
            }
        }
    });
    user.catch(err => {
        logger.log.error("...error while signing in user.", err, req.body.mobileno);
    })
}