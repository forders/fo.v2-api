import { Request, Response } from 'express';
import {
    userModel
} from '../../../../models';
import {
    respond,
    cquery
} from '../../../services'
import {
    logger
} from '../../../../modules'
import { res_msg } from '../../../../configs';

function getAll(req: Request, res: Response) {
    logger.log.info("Getting user list...");
    let lists = cquery.get(userModel, {}, { raw: true });
    lists.then(results => {
        logger.log.info("...user list.");
        respond(true, results, "List", "", res);
    })
    lists.catch(err => {
        logger.log.error("...unable to fetch user list.", err);
        respond(false, "", "List", err, res);
    })
}

function getById(req: Request, res: Response) {
    var role = cquery.findOne(userModel, {
        userid: Buffer.from(req.params.id, 'base64').toString()
    }, {
            allow_filtering: true
        });
    role.then(results => {
        logger.log.info("...role detail.");
        respond(true, results, "Detail", "", res);
    })
    role.catch(err => {
        logger.log.error("...unable to fetch role detail.", err, req.params.id);
        respond(false, "", "Detail", err, res);
    })
}

export {
    getAll,
    getById
}