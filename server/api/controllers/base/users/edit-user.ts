import { Request, Response } from 'express';
import {
    userModel
} from '../../../../models';
import {
    respond,
    cquery
} from '../../../services'
import {
    logger,
    db
} from '../../../../modules'
import { res_msg } from '../../../../configs';

export default function edituser(req: Request, res: Response) {
    let data = req.body;
    let id = Buffer.from(req.params.id, 'base64').toString();
    let q = {
        _key: db.uuifFromString(id)
    }
    let options = {
        ttl: 86400, if_exists: true
    }
    logger.log.info("Started Updating user...");
    let industry = cquery.updateOne(userModel, q, data, options)
    industry.then(() => {
        logger.log.info("...user updated successfully");
        respond(true, "",res_msg.UPDATESUCCESS.replace("{{}}","User"), "", res);
    });
    industry.catch(err => {
        logger.log.error("...user update failed",err);
        respond(false, "", res_msg.UPDATEFAILED.replace("{{}}","user"), err, res);
    })
}