import express from 'express';

import addUser from './add-user';
import editUser from './edit-user';
import {
    getAll,
    getById
} from './get-user'

export default express.Router()
    .post('/add',addUser)
    .post('/edit/:id',editUser)
    .get('/', getAll)
    .get('/:id', getById)