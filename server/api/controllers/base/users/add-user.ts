import { Request, Response } from 'express';
import {
    userModel
} from '../../../../models';
import {
    respond,
    cquery
} from '../../../services'
import {
    pwd,
    logger,
    db
} from '../../../../modules'
import { res_msg } from '../../../../configs';

export default function adduser(req: Request, res: Response) {

    let userid = db.genuuid();

    let newUser = req.body;
    newUser["password"] = pwd.encrypt(newUser['password']);
    newUser["_key"] = userid;
    newUser["userid"] = userid.toString()

    logger.log.info(`Adding new user...`);

    let exists = cquery.findOne(userModel, {
        mobileno: req.body.mobileno
    }, { allow_filtering: true })

    exists.then(results => {
        if (results == undefined) {
            let doc = cquery.addOne(userModel, newUser);
            doc.then((data) => {
                logger.log.info("...user added successfully");
                respond(true, "", res_msg.ADDSUCCESS.replace("{{}}", "User"), "", res);
            })
            doc.catch(err => {
                logger.log.error("...unable to add user", err, req.body);
                respond(false, "", res_msg.ADDFAILED.replace("{{}}", "user"), err, res);
            })
        } else {
            logger.log.info("...user already exists", req.body.mobilno);
            respond(false, "", res_msg.ADDFAILED.replace("{{}}", "user. Already exists"), "Already Exists", res);
        }
    });
    exists.catch(err => {
        logger.log.error("...verifying user exists add user.", err, req.body);
        respond(false, "", res_msg.ADDFAILED.replace("{{}}", "user"), err, res);
    })
}