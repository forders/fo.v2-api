import { Request, Response } from 'express';
import {
    tenantModel,
    roleModel
} from '../../../../models';
import {
    respond,
    cquery
} from '../../../services'
import {
    logger,
    db
} from '../../../../modules'
import { res_msg } from '../../../../configs';

export default function addtenant(req: Request, res: Response) {
    let newTenant = req.body;
    
    let tenantid = db.genuuid();
    let roleid = db.genuuid();
    let subscriptioid = db.genuuid();

    let newRole = {
        _key:roleid,
        roleid:roleid.toString(),
        rolename:"Admin",
        tenantid:tenantid,
        uiactions:[],
        dataaccess:"all",
        status:"Active",
        createdby:newTenant["createdby"],
        updatedby:newTenant["createdby"]
    }

    let newSubscription = {
        _key:subscriptioid,
        subscriptioid:subscriptioid.toString(),
        tenantid:tenantid,
        tenure:"Monthly",
        subsamount:"431",
        lastpaidamount:"431"
    }

    let tenant = cquery.addOne(tenantModel, newTenant);
    let role = cquery.addOne(roleModel,newRole);
}