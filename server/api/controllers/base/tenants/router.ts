import express from 'express';

import addTenant from './add-tenant';

export default express.Router()
    .post('/add',addTenant)