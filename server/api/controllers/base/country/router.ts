import express from 'express';

import addcountry from './add-country';

export default express.Router()
    .post('/add', addcountry);