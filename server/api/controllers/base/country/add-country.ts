import { Request, Response } from 'express';
import {
    countryModel
} from '../../../../models';
import {
    respond,
    cquery
} from '../../../services'
import {
    logger,
    db
} from '../../../../modules'
import { res_msg } from '../../../../configs';

export default function addcountry(req: Request, res: Response) {
    let newCountry = req.body;
    let uid = db.genuuid();
    newCountry["countryid"] = uid.toString();
    newCountry["_key"] = uid

    logger.log.info('Adding new country...');

    let doc = cquery.addOne(countryModel, newCountry);
    doc.then(() => {
        logger.log.info("...new country added", newCountry["countryname"]);
        respond(true, "", res_msg.ADDSUCCESS.replace("{{}}", "Country"), "", res);
    });
    doc.catch((err) => {
        logger.log.error("...error adding country", err, newCountry["countryname"]);
        respond(false, "", res_msg.ADDFAILED.replace("{{}}", "Country"), err, res);
    })
}