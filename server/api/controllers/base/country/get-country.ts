import { Request, Response } from 'express';
import {
    countryModel
} from '../../../../models';
import {
    respond,
    cquery
} from '../../../services'
import {
    logger
} from '../../../../modules'
import { res_msg } from '../../../../configs';

export default function newcountry(req: Request, res: Response) {
    let newCountry = req.body;

    logger.log.info('Adding new country');

    let doc = cquery.findOne(countryModel, {
            countryid: ""
        }, {
            
        });
    doc.then(() => {
        respond(true, "", res_msg.ADDSUCCESS.replace("{{}}", "Country"), "", res);
    });
    doc.catch((err) => {
        logger.log.error("Error -> Adding new country", err, req.body);
        respond(false, "", res_msg.ADDFAILED.replace("{{}}", "Country"), err, res);
    })
}