import express from 'express';

import addindustry from './add-industry';
import editindustry from './edit-industry';

export default express.Router()
    .post('/edit/:id', editindustry)
    .post('/add', addindustry);