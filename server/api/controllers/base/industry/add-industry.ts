import { Request, Response } from 'express';
import {
    industryModel
} from '../../../../models';
import {
    respond,
    cquery
} from '../../../services'
import {
    logger,
    db
} from '../../../../modules'
import { res_msg } from '../../../../configs';

export default function addindustry(req: Request, res: Response) {
    let newIndustry = req.body;
    let uid = db.genuuid();
    newIndustry["_key"] = uid;
    newIndustry["industryid"] = uid.toString();


    logger.log.info('Adding new industry...');

    let doc = cquery.addOne(industryModel, newIndustry);
    doc.then(() => {
        logger.log.info("...new industry added",newIndustry["industryname"]);
        respond(true, "", res_msg.ADDSUCCESS.replace("{{}}","Industry"), "", res);
    });
    doc.catch((err) => {
        logger.log.error("...error adding new industry", err, newIndustry["industryname"]);
        respond(false, "", res_msg.ADDFAILED.replace("{{}}","Industry"), err, res);
    });
}