import { Request, Response } from 'express';
import {
    industryModel
} from '../../../../models';
import {
    respond,
    cquery
} from '../../../services'
import {
    logger,
    db
} from '../../../../modules'
import { res_msg } from '../../../../configs';

export default function editindustry(req: Request, res: Response) {
    let data = req.body;
    let id = Buffer.from(req.params.id, 'base64').toString();
    let q = {
        industryid: id,
        _key: db.uuifFromString(id)
    }
    let options = {
        ttl: 86400, if_exists: true
    }
    logger.log.info("Started Updating industry...");
    let industry = cquery.updateOne(industryModel, q, data, options)
    industry.then(() => {
        logger.log.info("...Industry updated successfully");
        respond(true, "",res_msg.UPDATESUCCESS.replace("{{}}","Industry"), "", res);
    });
    industry.catch(err => {
        logger.log.error("...Industry update failed",err);
        respond(false, "", res_msg.UPDATEFAILED.replace("{{}}","Industry"), err, res);
    })
}